
About Minecraft2D
=================
In our DataStructures course (2018), we had to create a game using SFML and two self-made data structure (List and ResizableGrid). The game is mix up of minecraft/platformer/bulletHell/bossBattle. There is some finishing who would be possible, but in the current state, it is good enough.

Developers
----------
* Anthony Cote
* Catherine Bronsard
* Emile Loiselle

Requirements
------------
* C++
* SFML/Graphics
* SFML/Audio

Build requirements
------------------
Multi-threaded DLL
Subsystem:Window
Windows SDK Version 10.0.16299.0
Platform Toolset: Visual Studio 2017 v141

Building & Running
------------------
The game was build/run directly from VisualStudio 2017 for Windows.

Contributing
------------
Minecraft2D is not maintained, the scope was a school project and to showcase in our portfolio, so it require no further development. DeltaHit talked about working a bit on it so expect the latest development to be on his GitHub.

Links
-----
CoteAnthony's GitLab:   https://gitlab.com/CoteAnthony/minecraft2d
DeltaHit's GitHub:      https://github.com/DeltaHit/Minecraft2D
